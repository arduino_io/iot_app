import { StatusBar } from "expo-status-bar"
import { StyleSheet, View } from "react-native"
import Main from "./src"
import { SafeAreaView } from "react-native-safe-area-context"
import { RootSiblingParent } from "react-native-root-siblings"
import Context from "./src/data/context"
import { useState } from "react"
import { HubType } from "@arduino_io/types"

export default function App() {
	const [url, setUrl] = useState<string>("192.168.1.9:3000")
	const [hubs, setHubs] = useState<HubType[]>([])
	const [isUrlOk, setUrlOk] = useState<boolean>(false)
	return (
		<Context.Provider value={{ url, setUrl, hubs, setHubs, isUrlOk, setUrlOk }}>
			<SafeAreaView>
				<RootSiblingParent>
					<View style={styles.container}>
						<Main />
						<StatusBar style="dark" />
					</View>
				</RootSiblingParent>
			</SafeAreaView>
		</Context.Provider>
	)
}

const styles = StyleSheet.create({
	container: {
		marginTop: 5,
		marginHorizontal: 10,
		backgroundColor: "#fff",
		alignItems: "flex-start",
		justifyContent: "center",
	},
})
