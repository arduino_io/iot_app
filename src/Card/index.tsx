import { PropsWithChildren } from "react"
import { StyleSheet, Text, View } from "react-native"

interface ICard {
	title: string
}

function Card({ title, children }: PropsWithChildren<ICard>) {
	return (
		<View style={styles.container}>
			<Text>{title}</Text>
			{children}
		</View>
	)
}

const styles = StyleSheet.create({
	container: {
		borderColor: "#121212",
		borderWidth: StyleSheet.hairlineWidth,
		marginHorizontal: 5,
		marginVertical: 8,
		borderRadius: 5,
		paddingHorizontal: 4,
		paddingVertical: 5,
	},
})

export default Card
