import { HubType } from "@arduino_io/types"
import { View } from "react-native"
import Item from "./Item"
import Card from "../../Card"

interface IHub {
	data: HubType
}

function Hub({ data }: IHub) {
	return (
		<Card title={data.name}>
			{data.items.map((item) => (
				<Card key={item.name} title={item.name}>
					<Item data={item} hubName={data.name} />
				</Card>
			))}
		</Card>
	)
}

export default Hub
