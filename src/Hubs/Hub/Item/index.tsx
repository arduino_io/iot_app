import { StyleSheet, Text, View } from "react-native"
import { IoType, type ComponentType, DataType } from "@arduino_io/types"
import CheckBox from "expo-checkbox"
import Slider from "@react-native-community/slider"
import MaterialIcons from "@expo/vector-icons/MaterialIcons"
import useHub from "../../../service/useHub"

interface IItem {
	data: ComponentType
	hubName: string
}

function Item({ data: { io, data, value, name }, hubName }: IItem) {
	const { updateBooleanValue, updatePercentValue } = useHub()
	const isInput = io === IoType.IN

	async function onChangePercent(nextValue: number): Promise<void> {
		const value = 100 * nextValue
		await updatePercentValue(hubName, name, value)
	}

	async function onChangeBoolean(nextValue: boolean): Promise<void> {
		await updateBooleanValue(hubName, name, nextValue)
	}


	return (
		<View style={styles.item}>
			<MaterialIcons name={isInput ? "keyboard" : "mobile-screen-share"} size={20} />
			{typeof value === "boolean" && data === DataType.BOOLEAN ? (
				<CheckBox disabled={isInput} value={value} onTouchEnd={(evt) => {
					onChangeBoolean(!value)
				}} />
			) : (
				<Slider
					minimumValue={0}
					maximumValue={1}
					minimumTrackTintColor="#00ffff"
					maximumTrackTintColor="#000000"
					style={{ width: "98%", height: 40 }}
					onValueChange={onChangePercent}
					value={value as number} // here it must be 'number'.
				/>
			)}
		</View>
	)
}

const styles = StyleSheet.create({
	item: {
		paddingTop: 5,
		height: 35,
		alignItems: "center",
		flexDirection: "row",
	},
})

export default Item
