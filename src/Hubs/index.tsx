import { useContext, useState } from "react"
import { FlatList, RefreshControl } from "react-native"
import EmptyList from "../EmptyList"
import Hub from "./Hub"
import Context from "../data/context"
import useHub from "../service/useHub"

function Hubs() {
	const [isLoading, setLoading] = useState<boolean>(false)
	const { hubs } = useContext(Context)
	const { updateHubsData } = useHub()

	async function callForMore() {
		setLoading(true)
		await updateHubsData()
		setLoading(false)
	}

	return (
		<FlatList
			refreshing={isLoading}
			onRefresh={callForMore}
			data={hubs ?? []}
			keyExtractor={(item) => item.name}
			ListEmptyComponent={() => <EmptyList onPress={callForMore} />}
			renderItem={(hub) => <Hub data={hub.item} key={hub.item.name} />}
			refreshControl={<RefreshControl refreshing={isLoading} onRefresh={callForMore} />}
		/>
	)
}

export default Hubs
