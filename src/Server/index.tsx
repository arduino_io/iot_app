import { Button, StyleSheet, TextInput, View } from "react-native"
import Card from "../Card"
import useServer from "./useServer"
import Toast from "react-native-root-toast"
import { useContext, useState } from "react"
import Context from "../data/context"

function Server() {
	const { onConnect, isLoading } = useServer()
	const { url, setUrl } = useContext(Context)

	return (
		<Card title="Server">
			<View style={styles.ip}>
				<TextInput defaultValue={url} editable={!isLoading} onChangeText={setUrl} placeholder="Server ip: ___.___.___.___" />
				<Button title="Connect" disabled={isLoading} onPress={onConnect} />
			</View>
		</Card>
	)
}

const styles = StyleSheet.create({
	ip: {
		flexDirection: "row",
		justifyContent: "space-between",
	},
})

export default Server
