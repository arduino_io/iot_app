import { useContext, useState } from "react"
import Context from "../data/context"
import Toast from "react-native-root-toast"
import { checkConnection, updateUrl, validate } from "../util"
import useHub from "../service/useHub"

interface Out {
	onConnect: () => Promise<void>
	isLoading: boolean
}

function useServer(): Out {
	const { url, setUrlOk, setUrl } = useContext(Context)
	const [isLoading, setLoading] = useState<boolean>(false)
	const { updateHubsData } = useHub()

	async function onConnect() {
		const _url = updateUrl(url)
		try {
			setLoading(true)
			setUrlOk(false)
			if (validate(_url)) {
				const valid = await checkConnection(_url)
				setUrlOk(valid)

				if (valid) {
					setUrl(_url)
					await updateHubsData()
				}
			} else {
				Toast.show("The address is invalid", {
					duration: Toast.durations.LONG,
				})
			}
		} catch (error) {
			console.error(error)
		} finally {
			setLoading(false)
		}
	}

	return { onConnect, isLoading }
}

export default useServer
