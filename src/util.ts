/*
 * 192.168.1.0
 * http://iago.com
 * http://iago.iago.com
 * https://iago.com
 * https://iago.iago.com
 */
const mask = /(\d{1,3}.){3}\d{1,3}:\d+|http(s?):\/\/(\w+\.)+\w+:\d+/

function hasHttp(address: string): boolean {
	return address.indexOf("http") === 0
}

function updateUrl(url: string): string {
	return hasHttp(url) ? url : `http://${url}`
}

async function checkConnection(url: string): Promise<boolean> {
	const _url = updateUrl(url)
	const result = await fetch(_url, { method: "HEAD" })
	return result.ok
}

function validate(ip: string): boolean {
	return mask.test(ip)
}

export { updateUrl, checkConnection, validate }
