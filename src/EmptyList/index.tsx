import { Button } from "react-native"

interface IEmptyList {
	onPress: () => void
}
function EmptyList({ onPress }: IEmptyList) {
	return <Button title="Re-load" onPress={onPress} />
}

export default EmptyList
