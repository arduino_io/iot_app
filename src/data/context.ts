import { HubType } from "@arduino_io/types"
import { createContext } from "react"

interface AppContext {
	url: string
	readonly setUrl: (url: string) => void
	hubs?: HubType[]
	readonly setHubs: (hub: HubType[]) => void
	isUrlOk: boolean
	readonly setUrlOk: (ok: boolean) => void
}

const Context = createContext<AppContext>(null)

export default Context
