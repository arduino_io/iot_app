import { Hub, HubType } from "@arduino_io/types"
import { create } from "superstruct"

interface Out {
	describe: (url: string, hubName?: string) => Promise<HubType[]>
	setValue:(url:string, hubName:string, componentName:string, value:number|boolean)=>Promise<void>
}

// biome-ignore lint/suspicious/noExplicitAny: <explanation>
async function fetcher<T>(url: string, converter: (d: any) => T) {
	try {
		const response = await fetch(url)
		console.log({url})
		if (response.status !== 404) {
			const data = await response.json()

			return converter(data)
		}
	} catch (error) {
		return null
	}
	return null
}

function useFetch(): Out {
	async function describe(url: string, hubName = ""): Promise<HubType[]> {
		return await fetcher(`${url}/describe/${hubName}`, (data: HubType[]) => data.map((item) => create(item, Hub)))
	}

	async function setValue(url:string, hubName:string, componentName:string, value:number|boolean):Promise<void>{
		const _value = typeof value === 'number' ? `${value}`: (value ? "true":"false")

		await fetcher(`${url}/write/${hubName}/${componentName}/${_value}`, ()=>{})
	}
	return { describe,setValue }
}

export default useFetch
