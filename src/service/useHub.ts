import { useContext } from "react"
import Context from "../data/context"
import useFetch from "./useFetch"

interface Out {
	updateHubsData: () => Promise<void>
	updatePercentValue:(hubName:string, componentName:string, value:number)=>Promise<void>
	updateBooleanValue:(hubName:string, componentName:string, value:boolean)=>Promise<void>
}
function useHub(): Out {
	const { url, setHubs } = useContext(Context)
	const { describe,setValue } = useFetch()

	async function updateHubsData(): Promise<void> {
		const hubs = await describe(url)
		setHubs(hubs)
	}

	async function updatePercentValue(hubName:string, componentName:string, value:number):Promise<void> {
		await setValue(url, hubName, componentName, value)
	}
	
	async function updateBooleanValue(hubName:string, componentName:string, value:boolean):Promise<void> {
		await setValue(url, hubName, componentName, value)
		await updateHubsData()
	}


	return { updateHubsData, updateBooleanValue, updatePercentValue }
}

export default useHub
