import { StyleSheet, View } from "react-native"
import Hubs from "./Hubs"
import Server from "./Server"
import { useContext } from "react"
import Context from "./data/context"

function Main() {
	const { isUrlOk } = useContext(Context)
	return (
		<View style={styles.container}>
			<Server />
			{isUrlOk && <Hubs />}
		</View>
	)
}

const styles = StyleSheet.create({
	container: {
		width: "100%",
		display: "flex",
	},
})

export default Main
